import scrapy
from scrapy.loader import ItemLoader
import json
import unicodedata
import string
from scrapy.http import HtmlResponse
from scrapy.selector import Selector
from searchchscraper.items import SearchchscraperItem



all_letters = string.ascii_letters + " _-.,;'"+string.digits+'"'+string.ascii_uppercase
n_letters = len(all_letters)

def unicodeToAscii(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn' and c in all_letters)




class SearchSpider(scrapy.Spider):
    name="searchch"
    item_type='restaurant'
    counter=0

    def start_requests(self):
        self.item_type=self.type
        base_url="https://map.search.ch/api/poi_html.json?mode=hover&mx1={x1}&mx2={x2}&my1={y1}&my2={y2}&zd=512&poi={type}&lang=en&base=485000,296000"
        for i in range(100000,800000,100000):
            for j in range(100000,400000,100000):
                url_to_scrape=base_url.replace('{x1}',str(i)).replace('{x2}',str(i+100000)).replace('{y1}',str(j)).replace('{y2}',str(j+100000)).replace('{type}',self.item_type)
                request=scrapy.Request(url=url_to_scrape,callback=self.parse_geo)
                yield request

    def parse_geo(self,response):
        data=json.loads(response.body_as_unicode())
        for d in data['pois']:
            name=d['name']
            name=unicodeToAscii(name[:name.find('<')])
            x=d["x"]
            y=d["y"]
            item_url="https://map.search.ch/api/poi_html.json?x={x}&y={y}&zd=512&poi={type}&lang=en"
            item_request=scrapy.Request(url=item_url.replace('{x}',x).replace('{y}',y).replace('{type}',self.item_type),callback=self.parse_info)
            item_request.meta['x']=x
            item_request.meta['y'] = y
            item_request.meta['name'] = name
            yield item_request

    def parse_info(self,response):
        name=response.meta['name']
        x=response.meta['x']
        y=response.meta['y']
        data=json.loads(response.body_as_unicode())
        html=data['pois']
        phone_class=Selector(text=html).css('p.phone_number')
        phone=phone_class[0].css('a::attr(href)').extract_first()
        phone=phone.replace('//tel.hilma.search.ch/voip.html?tel=','tel:')
        website=Selector(text=html).css('a.sl-icon-website::attr(href)').extract_first()
        address=Selector(text=html).css('p.map-location-address::text').extract_first()
        try:
            address=unicodeToAscii(address)
        except:
            pass
        print(str(self.counter))
        self.counter+=1
        l = ItemLoader(item=SearchchscraperItem(), response=response)
        l.add_value('name', name)
        l.add_value('x', x)
        l.add_value('y', y)
        l.add_value('website', website)
        l.add_value('address', address)
        l.add_value('phone',phone)
        yield l.load_item()
