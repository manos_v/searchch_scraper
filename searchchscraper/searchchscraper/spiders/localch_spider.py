import scrapy
from scrapy.loader import ItemLoader
import json
import unicodedata
import string
from scrapy.http import HtmlResponse
from scrapy.selector import Selector
from searchchscraper.items import SearchchscraperItem



all_letters = string.ascii_letters + " _-.,;'"+string.digits+'"'+string.ascii_uppercase
n_letters = len(all_letters)

def unicodeToAscii(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn' and c in all_letters)




class SearchSpider(scrapy.Spider):
    name="localch"
    item_type='Restaurant'
    counter=0

    def start_requests(self):
        self.item_type=self.type
        base_url="https://www.local.ch/en/map/api/q.json?layers=results&what={type}&where=&bounds={x1}%2C{y1}%2C{x2}%2C{y2}&zoom=1&serializedQuery=&point=&transportLimit=0"

        for i in range(1, 100, 1):
            for j in range(1, 100, 1):
                url_to_scrape = base_url.replace('{x1}', str(i)).replace('{x2}', str(i + 1)).replace('{y1}',
                                                                                                          str(j)).replace(
                    '{y2}', str(j + 1)).replace('{type}', self.item_type)
                # print(url_to_scrape)
                request = scrapy.Request(url=url_to_scrape, callback=self.parse_ids)
                yield request

    def parse_ids(self,response):
        data = json.loads(response.body_as_unicode())
        for d in data['pois']:
            listing_url='https://www.local.ch/en/map/api/d/{id}.json'
            request = scrapy.Request(url=listing_url.replace('{id}',d['id']), callback=self.parse_info, )
            yield request


    def parse_info(self,response):
        data=json.loads(response.body_as_unicode())
        name=unicodeToAscii(data['name'])
        x=data['lat']
        y=data['lng']

        website=''
        address=unicodeToAscii(', '.join(data['address']))
        phone=''
        email=''
        for d in data['contacts']:
            if d['type']=='phone':
                phone=Selector(text=d['html']).css('a::text').extract_first()
            elif d['type']=='email':
                email = Selector(text=d['html']).css('a::text').extract_first()
            elif d['type']=='website':
                website = Selector(text=d['html']).css('a::text').extract_first()

        print(self.counter)
        self.counter+=1
        l = ItemLoader(item=SearchchscraperItem(), response=response)
        l.add_value('name', name)
        l.add_value('x', x)
        l.add_value('y', y)
        l.add_value('website', website)
        l.add_value('address', address)
        l.add_value('phone',phone)
        l.add_value('email', email)
        yield l.load_item()
